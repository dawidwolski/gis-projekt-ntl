#ifndef NTL_H
#define NTL_H

#include "graph.h"

class NTL
{
public:
    NTL(Graph*);
    void colorNC();
    void colorNTL();
private:
    Graph *graph;
};

#endif // NTL_H
