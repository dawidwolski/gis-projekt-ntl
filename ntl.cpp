#include "ntl.h"
#include "QDebug"
#include "QTime"
#include <iostream>
NTL::NTL(Graph *graph)
{
    this->graph = graph;
}

void NTL::colorNC(){
    QTime time;
    time.start();
    this->graph->setUsedColors(1);
    foreach (Node* node, graph->getNodes()) {
        qDebug()<<"wierzcholek "<<node->getNumber();
        foreach (Edge* edge, node->getEdges()) {
            if(edge->getColor() <0){
                int newColor = this->graph->lowestFree(edge);
                if(newColor < 0){
                    int newColor = this->graph->getUsedColors()+1;
                    this->graph->setUsedColors(newColor);
                    edge->setColor(newColor-1);

                }else{
                    edge->setColor(newColor);
                }
            }
        }
    }

    qDebug()<<"NC: Czas obliczen dla "<<this->graph->getNoOfNodes() << " wierzchołków i " <<
              this->graph->getNoOfEdges() << " krawędzi wynosi " << time.elapsed() << " ms";
    qDebug()<<"Znalezione kolory "<<this->graph->getUsedColors();
}

void NTL::colorNTL(){
    QTime time;
    time.start();

    //Minimalna liczba kolorów to największy stopień wierzchołka
    foreach (Node* node, graph->getNodes()) {
        if(node->getEdges().length() > this->graph->getUsedColors()){
            this->graph->setUsedColors(node->getEdges().length());
            //qDebug()<<"wierzcholek "<<node->getNumber();
        }
    }

    //qDebug()<<"najwiekszy stopien wierzcholka " << this->graph->getUsedColors();

    foreach (Node* node, graph->getNodes()) {
        qDebug()<<"Wierzcholek "<< node->getNumber()<<"/"<<graph->getNodes().size();
        foreach (Edge* edge, node->getEdges()) {
            if(edge->getColor() == -1){
                int newColor = this->graph->lowestFree(edge);
                if(newColor < this->graph->getUsedColors() && newColor > -1){
                    edge->setColor(newColor);
                }else{
                    //Tworzymy wachlarz przy analizowanym wierzchołku
                    QList<Edge*> fan;
                    fan.push_front(edge);
                    foreach (Edge* edge, node->getEdges()) {
                        if(edge != fan.first() && edge->getColor() != -1){
                            fan.push_back(edge);
                        }
                    }

                    foreach (Edge* tmpEdge, fan) {
                        newColor = this->graph->lowestFree(tmpEdge);
                        if(newColor != -1){
                            tmpEdge->setColor(newColor);
                            int tmpColor = this->graph->lowestFree(edge);
                            if(tmpColor != -1){
                                edge->setColor(tmpColor);
                                break;
                            }
                        }
                    }

                    if(edge->getColor() == -1){
                        this->graph->setUsedColors(this->graph->getUsedColors()+1);
                        int tmpColor = this->graph->lowestFree(edge);
                        edge->setColor(tmpColor);
                    }
                }
            }
        }
    }

    qDebug()<<"NTL: Czas obliczen dla "<<this->graph->getNoOfNodes() << " wierzchołków i " <<
              this->graph->getNoOfEdges() << " krawędzi wynosi " << time.elapsed() << " ms";
    qDebug()<<"Znalezione kolory "<<this->graph->getUsedColors();
}
