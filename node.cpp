#include "node.h"

Node::Node()
{
    this->number = -1;
}

Node::Node(int number){
    this->number = number;
}

void Node::appendEdge(Edge *edge){
    this->edges.append(edge);
}

int Node::howManyEdges(){
    return this->edges.length();
}
QList<Edge *> Node::getEdges() const
{
    return edges;
}
int Node::getNumber() const
{
    return number;
}

