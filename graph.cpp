#include "graph.h"
#include "QFile"

#include <QTextStream>

void Graph::process_line(QString line){
    //Pominięcie komentarzy
    if(line[0] == 'c'){
        return;
    }

    QStringList command = line.split(" ");

    if(command.length() < 2){
        return;
    }

    QString temp = command.at(0);
    if(temp == "p"){
        temp = command.at(1);
        if(command.length() == 4 && temp == "edge"){
            temp = command.at(2);
            this->setNoOfNodes(temp.toInt());
            temp = command.at(3);
            this->setNoOfEdges(temp.toInt());
        }else{
            return;
        }
    }else
    if(temp == "e"){
        if(command.length() == 3){
            temp = command.at(1);
            int nodeA = temp.toInt();
            temp = command.at(2);
            int nodeB = temp.toInt();
            Edge *tempEdge = new Edge(nodeA, nodeB);
            if(nodes.contains(nodeA)){
                Node *tempNode = nodes.value(nodeA);
                tempNode->appendEdge(tempEdge);
            }else{
                Node *tempNode = new Node(nodeA);
                tempNode->appendEdge(tempEdge);
                nodes.insert(nodeA, tempNode);
            }

            if(nodes.contains(nodeB)){
                Node *tempNode = nodes.value(nodeB);
                tempNode->appendEdge(tempEdge);
            }else{
                Node *tempNode = new Node(nodeB);
                 tempNode->appendEdge(tempEdge);
                nodes.insert(nodeB, tempNode);
            }
        }
    }
}


Graph::Graph(QString path)
{
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        process_line(line);
    }

    this->usedColors = -1;
}

int Graph::lowestFree(Edge *edge){
    QMap<int, int> result;
    foreach (Edge* secondEdge, this->getNodes().value(edge->getNodeA())->getEdges() ) {
        result.insert(secondEdge->getColor(),1);
    }
    foreach (Edge* secondEdge, this->getNodes().value(edge->getNodeB())->getEdges()) {
        result.insert(secondEdge->getColor(),1);
    }

    for(int i=0; i<this->getUsedColors(); ++i){
        if( !result.contains(i) ){
            return i;
        }
    }
    return -1;
}


int Graph::getNoOfNodes() const
{
    return noOfNodes;
}

void Graph::setNoOfNodes(int value)
{
    noOfNodes = value;
}
int Graph::getNoOfEdges() const
{
    return noOfEdges;
}

void Graph::setNoOfEdges(int value)
{
    noOfEdges = value;
}
QMap<int, Node *> Graph::getNodes()
{
    return nodes;
}
int Graph::getUsedColors() const
{
    return usedColors;
}

void Graph::setUsedColors(int value)
{
    usedColors = value;
}

