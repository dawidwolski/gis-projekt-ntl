#ifndef GRAPH_H
#define GRAPH_H

#include <QList>
#include <QString>
#include <QMap>
#include "node.h"
//#include "edge.h"

class Graph
{
public:
    Graph(QString);
    int getNoOfNodes() const;
    void setNoOfNodes(int value);

    int getNoOfEdges() const;
    void setNoOfEdges(int value);

    QMap<int, Node*> getNodes();

//    QList<Edge> getEdges() const;

//    void append(Edge*);

    int getUsedColors() const;
    void setUsedColors(int value);
    int lowestFree(Edge*);

private:
    int noOfNodes;
    int noOfEdges;
    QMap<int, Node*> nodes;
    int usedColors;

    void process_line(QString);
};

#endif // GRAPH_H
