#-------------------------------------------------
#
# Project created by QtCreator 2015-05-16T13:53:59
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = nc_ntl
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    edge.cpp \
    node.cpp \
    graph.cpp \
    ntl.cpp

HEADERS += \
    edge.h \
    node.h \
    graph.h \
    ntl.h
