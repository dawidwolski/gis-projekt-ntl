#include "edge.h"

Edge::Edge()
{

}

Edge::Edge(int nodeA, int nodeB){
    this->nodeA = nodeA;
    this->nodeB = nodeB;
    this->color = -1;
}

Edge::Edge(int nodeA, int nodeB, int color){
    this->nodeA = nodeA;
    this->nodeB = nodeB;
    this->color = color;
}

bool Edge::operator==(const Edge& e){
    if(this->getNodeA() == e.getNodeA() && this->getNodeB() == e.getNodeB() ){
        return true;
    }else{
        return false;
    }
}

int Edge::getNodeA() const
{
    return nodeA;
}
int Edge::getNodeB() const
{
    return nodeB;
}
int Edge::getColor() const
{
    return color;
}

void Edge::setColor(int value)
{
    color = value;
}



