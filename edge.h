#ifndef EDGE_H
#define EDGE_H

class Edge
{
public:
    Edge();
    Edge(int, int);
    Edge(int, int, int);
    int getNodeA() const;

    int getNodeB() const;

    int getColor() const;
    void setColor(int value);

    bool operator==(const Edge&);

private:
    int nodeA;
    int nodeB;
    int color;
};

#endif // EDGE_H
