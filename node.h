#ifndef NODE_H
#define NODE_H

#include <QList>
#include "edge.h"

class Node
{
public:
    Node();
    Node(int);
    void appendEdge(Edge*);
    int howManyEdges();
    QList<Edge *> getEdges() const;

    int getNumber() const;

private:
    int number;
    QList<Edge*> edges;
};

#endif // NODE_H
